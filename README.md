# Certifikáty Let's Encrypt v prostředí FI MU

## Potřebuji tohle?

Možná ne :-). Certifikační autorita [Let's Encrypt](https://letsencrypt.org)
je přístupná komukoli.
Jedna z možností tedy je celé to nastudovat, zprovoznit některého klienta
a řídit si vystavování a obnovování certifikátů ve vlastní režii, bez
spolupráce `unix@fi`.

Druhou možností je pořídit si certifikát Sectigo, který má univerzita zdarma
[přes CESNET](https://tcs.cesnet.cz/). Oproti Let's Encrypt je to o něco víc byrokracie, ale vydávají
certifikáty s platností na jeden rok (LE jen na tři měsíce).

Certifikát od LE je získatelný s menší byrokracií
a část práce s vystavením a obnovováním certifikátů může převzít `unix@fi`
za pomoci zde přístupných skriptů.

## Chci certifikát! Jak na to?

Příklad: potřebuji certifikát pro svůj nový projekt `https://zverinec.cz`, který
chci provozovat na svém serveru `zoo.fi.muni.cz` (projekt a server
ale nemusí nutně mít různá jména, může jít o totéž). Případně viz také sekci
*Více domén nebo přidání domény* níže.

* Na počítači, kde tento projekt má běžet, zkonfigurujte a spusťte HTTP server,
například Apache (HTTPS zatím ne, nemáte certifikát). Ujistěte se,
že po zadání (`http://zverinec.cz/`) do prohlížeče dostanete stránku ze svého
serveru.

* Při vydávání certifikátu je v pozdějším kroku vystavován přes webserver
challenge, který musí být přístupný po HTTP (na portu TCP/80) z celého světa. Proto je nutné
mít od `unix@fi` na fakultním firewallu povolený přístup přes HTTP(S) na daný
stroj.

* Zkopírujte si na svůj server tento repozitář:

    ```shell
    git clone https://gitlab.fi.muni.cz/unix/letsencrypt-fi.git
    ```

* Zkontrolujte, že na počítači jsou nainstalované balíky `openssl`, `perl` a `openssh-server`.

* Vygenerujte tajný klíč a certificate request (CSR) skriptem
`./le-gen-csr zverinec.cz`. Výstupem tohoto skriptu jsou čtyři soubory:

  * `zverinec.cz.key` – tajný klíč, který nikomu nedávejte. Zkopírujte jej do příslušného systémového adresáře (na RHEL/CentOS/Fedoře do `/etc/pki/tls/private/`, na Debianu/Ubuntu do `/etc/ssl/private/`). Ujistěte se, že všechny kopie klíče mají dostatečně striktně nastavená přístupová práva (například `chmod go= zverinec.cz.key`).
  * `zverinec.cz.crt` – dočasný self-signed certifikát, určený k tomu, abyste si mohli všechno nakonfigurovat a nemuseli čekat na vystavení "ostrého" certifikátu.
I tento umístěte do příslušného systémového adresáře, například do `/etc/pki/tls/certs/` nebo `/etc/ssl/certs/`.
  * `zverinec.cz-bundle.crt` – dočasný balík kořenových a mezilehlých certifikátů učený k testování, umístěte jej do adresáře s dočasným certifikátem.
  * `zverinec.cz-chain.crt` – kombinace dvou předchozích (tj. řetěz certifikátů od koncového po kořenový).
  * `zverinec.cz.csr` – certificate request. Na základě tohoto vystaví LE skutečný certifikát.

* Nakonfigurujte HTTPS server tak, aby používal výše uvedený klíč, certifikát a balík.
Pro Apache 2.4.8 a novější můžete použít tyto direktivy v souboru `httpd.conf`:

    ```apache
    SSLCertificateFile /etc/pki/tls/certs/zverinec.cz-chain.crt
    SSLCertificateKeyFile /etc/pki/tls/private/zverinec.cz.key
    ```
  U starších Apache 2.4 je potřeba oddělit koncový certifikát od mezilehlých:

    ```apache
    SSLCertificateFile /etc/pki/tls/certs/zverinec.cz.crt
    SSLCertificateChainFile /etc/pki/tls/certs/zverinec.cz-bundle.crt
    SSLCertificateKeyFile /etc/pki/tls/private/zverinec.cz.key
    ```

  Třeba pro Nginx zas použijte tyto direktivy:

    ```
    ssl_certificate /etc/pki/tls/certs/zverinec.cz-chain.crt
    ssl_certificate_key /etc/pki/tls/private/zverinec.cz.key
    ```

* Ověřte si úroveň bezpečnosti konfigurace SSL, například dle návodu na <https://www.fi.muni.cz/tech/unix/ssl-apache.html> (zakázání slabých šifer, zranitelných protokolů a podobně).

* Vytvořte přesměrování URL `/.well-known/acme-challenge` na server `fadmin.fi.muni.cz`. Pro Apache 2.4 můžete použít následující direktivu
v souboru `httpd.conf`:

    ```apache
    Redirect temp /.well-known/acme-challenge/ https://fadmin.fi.muni.cz/.well-known/acme-challenge/

    ```

  A pro Nginx zas následovnou:

    ```
    location ~ ^/\.well-known/acme-challenge/ {
        return 302 https://fadmin.fi.muni.cz/$request_uri;
    }
    ```

* Alternativně místo tohoto přesměrování a HTTP validace můžete použít
  DNS validaci. V případě hostname pod `.fi.muni.cz` prostě napište
  na `unix@fi`, že chcete použít DNS validaci. V případě vlastní domény
  je třeba nejdřív vytvořit CNAME záznam

    ```
    _acme-challenge.zverinec.cz. IN CNAME zverinec.cz._le.fi.muni.cz.
    ```

* Nainstalujte skript `le-renewed-cert` do systémových
adresářů a povolte k němu přístup přes SSH z počítače `thetis.fi.muni.cz`:

    ```shell
    cp le-renewed-cert /usr/local/sbin/
    cat letsencrypt-fi-authorized-keys >> /root/.ssh/authorized_keys
    ```

* Počítač `zoo.fi.muni.cz` musí mít ve Fakultní administrativě svůj [klíč SSH
  mezi známými](https://fadmin.fi.muni.cz/noauth/sshkh/ssh-known-hosts.mpl),
  aby se k němu byla `thetis` ochotna připojit a zaslat certifikáty. [Aktivaci
  klíče](https://fadmin.fi.muni.cz/auth/sit/ssh.mpl) může provést správce
  stroje.

* Ve skriptu je na začátku konfigurace, kde můžete upravit například
jiné umístění souboru s certifikáty nebo příkazu
pro znovunačtení konfigurace HTTPS serveru. Nepotřebujete-li generovat sloučený
řetěz certifikátů, můžete jeho generování vypnout (`$create_chain`).

* Nyní už zbývá jen požádat `unix@fi` o vystavení certifikátu. K žádosti přiložte
dříve vygenerovaný certificate request – soubor `zverinec.cz.csr`:

> Prosím o vygenerování a průběžnou aktualizaci certifikátu Let's Encrypt
> pro můj projekt `zverinec.cz`.
> Skripty z GitLab repozitáře `unix/letsencrypt-fi`
> jsou přístupné přes SSH na uživatele `root@zoo.fi.muni.cz`.
> Přikládám certificate request.
> Kontaktní adresa na správce tohoto projektu je `zverinec-devel@fi.muni.cz`.

* Poté `unix@fi` zajistí vystavení certifikátu a již pomocí nainstalovaných skriptů přes SSH se certifikát dostane na váš server.

* Skripty samozřejmě můžete zkoumat a upravovat dle libosti.

## Více domén nebo přidání domény

Pokud chcete certifikát pro více domén, v postupu výše vygenerujte CSR
s dalšími Subject Alternative Names (SAN):

    ./le-gen-csr zverinec.cz zoo.fi.muni.cz broukoviste.fi.muni.cz ...

A potřebné kroky aplikujte pro každou doménu.

V případě potřeby přidání domény je to velmi podobné. Vygenerujete nový
CSR – vznikne zároveň i nový privátní klíč apod. (Starý klíč a certifikát
v případě úspěšného dokončení postupu nebude potřeba.) Pak aplikujte potřebné
kroky pro novou doménu a požádejte `unix@fi` o vystavení certifikátu.

## Dotazy, připomínky

Máte-li dotazy nebo potřebujete-li ještě něco trochu jiného, než je zde popsáno,
neváhejte se obrátit na `unix@fi.muni.cz`.
